# Basic Bootstrap

1. Basic HTML Structure for Boostrap with jquery, popper and fontowsome
2. Container classes: container, container-fluid
3. 6 heading classes, 4 display classes, lead, bold, italic, lowercase, uppercase, capitalize, blockquote, text-aligns, lists
4. positions: text align, float, sticky, fixed
5. colors: text, baground, invisible
6. margin, paddings, center align, width, height, border
7. buttons : button and outline colors, size, fontowsome,button states, toggle button, dropdown buttons, button groups horizonal, vertical
8. navbar, responsive with button